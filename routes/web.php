<?php

use App\Http\Controllers\UserController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\EventRequestController;
use App\Http\Controllers\GameController;
use App\Http\Controllers\MapOfEventsController;


Route::get('/', function () {
    
    return view('home');

})->name('home');


Auth::routes();

Route::get('/profile', 'UserController@profile')->name('profile');

Route::post('/profile', 'UserController@update_avatar')->name('updateprofile');

Route::get('/event/create', 'EventController@create')->name('createevent');

Route::post('/event/store', 'EventController@store')->name('storeevent');

Route::get('/game/create', 'GameController@create')->name('creategame');

Route::post('/game/store', 'GameController@store')->name('storegame');

Route::get('/event/details', 'EventController@eventdetails')->name('eventdetails');

Route::get('/event/message', 'EventRequestController@eventmessageget')->name('eventmessage');

Route::post('/event/message', 'EventRequestController@eventmessagepost')->name('eventmessagepost');

Route::get('/event/request/details', 'EventRequestController@eventrequestdetails')->name('eventrequestsget');

Route::post('event/confirm', 'EventRequestController@confirmrequest')->name('confirmrequest');

Route::post('event/reject', 'EventRequestController@rejectrequest')->name('rejectrequest');

Route::get('/game/join', 'MapOfEventsController@joinagame')->name('joinagame')->middleware('auth');

Route::get('/event/city', 'MapOfEventsController@findeventonmap')->name('cityevents');

Route::get('/event/find', 'MapOfEventsController@findeventinputselect')->name('findevent');

Route::get('/event/user', 'EventController@userevents')->name('userevents');

Route::get('/event/hosted', 'EventController@hostedevents')->name('hostedevents');

Route::get('/event/requested', 'EventController@requestedevents')->name('requestedevents');

