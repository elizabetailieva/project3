@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row user-profile-row">
        <div class="user-profile-holder">
            <img id="profile" class="profile-picture" src="/img/{{ $user->avatar }}">
            <h2>{{ $user->name }}</h2>
            <form enctype="multipart/form-data" action="/profile" method="POST">
                <label for="file" >Change profile picture...</label>
                <input type="file" name="avatar" id="file" class="inputfile" onchange="readURL(this);">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="submit" value="Save profile" class="save-button">
            </form>
        </div>
    </div>
</div>

@endsection
