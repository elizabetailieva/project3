@extends('layouts.master')

@section('content')

<div class="container-gray">
    <div class="container-form">
        <h3>Sign Up</h3>
        <form method="POST" action="{{ route('register') }}">
            @csrf
            <div class="form-group">
                <label for="name">Name*</label>
                <input id="name" name="name" class="form-control custom-input{{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{old('name')}}" type="text">
                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="email">Email*</label>
                <input id="email" name="email" class="form-control custom-input{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{old('email')}}" type="email">
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="password">Password*</label>
                <input id="password" name="password" class="form-control custom-input{{ $errors->has('password') ? ' is-invalid' : '' }}" type="password">
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="password-confirm">Password Confirmation*</label>
                <input id="password-confirm" name="password_confirmation" class="form-control custom-input" type="password">
            </div>
            <input type="submit" class="btn btn1 btn-danger form-control" value="Sign Up">
            <div class="m-t-md"><p class="text-center">Got an account? <a href="{{ route('login') }}" class="red-link">Log in</a></p></div>
        </form>
    </div>
 </div>

@endsection
