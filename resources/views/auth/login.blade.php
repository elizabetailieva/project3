@extends('layouts.master')

@section('content')

    <div class="container-gray">
       <div class="container-form">
           <h3>Log in</h3>
           <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group">
                    <label for="email">Email*</label>
                    <input id="email" name="email" class="form-control custom-input{{ $errors->has('email') ? ' is-invalid' : '' }}" type="email">
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="password">Password*</label>
                    <input id="password" name="password" class="form-control custom-input{{ $errors->has('password') ? ' is-invalid' : '' }}" type="password">
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <input type="checkbox"><label class="m-l-s">Remember me</label>
                </div>
                <input type="submit" class="btn btn1 btn-danger form-control" value="Log in">
                <div class="m-t-md"><p class="text-center">New? <a href="{{ route('register') }}" class="red-link">Sign up</a><br>
                <a class="red-link" href="{{route('password.request')}}">Forgot your password?</a></p></div>
           </form>

       </div>
    </div>

@endsection
