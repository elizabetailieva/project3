<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <i class="fas fa-bars"></i>
        </button>
        <img class="navbar-brand" src="{{asset('img/onboard.png')}}"><a class="navbar-brand" href="{{route('home')}}">On Board</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right links">
                @guest
                    <li><a href="{{ route('login') }}"><span class="text-decorated">Login</span></a></li>
                    <li><a href="{{ route('register') }}"><span class="text-decorated">Sign Up</span></a></li>    
                @else
                    <li><a href="{{route('createevent')}}"><span class="text-decorated">Create an event</span></a></li>
                    <li><a href="{{route('userevents')}}"><span class="text-decorated my-events">My events</span></a></li>

                    <li class="dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="" role="button" data-toggle="dropdown"  >
                        <img src="/img/{{ Auth::user()->avatar }}" style="width:30px; height:30px; border-radius:50%">
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="{{ route('profile') }}"><span class="text-decorated">My profile</span></a></li>
                            <li><a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                <span class="text-decorated">Logout</span>
                            </a></li>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </ul>
                    </li>
                @endguest

            </ul>
        </div>
    </div>
</nav> 