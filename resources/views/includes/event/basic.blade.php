@foreach ($events as $event)
    <div class="col-md-12">
        <div class="row event-card event-card-basic">
            <div class="col-md-5">
                <div class="game-picture">
                    <img class="img-responsive" src="{{asset('storage/'.$event->game->picture)}}">
                </div>
            </div>
            <div class="col-md-7 event-details-text-holder">
                <div class="event-details-text event-basic">
                    <p><b>Game:</b> {{$event->game->name}}</p>
                    <p><b>Host:</b> {{$event->host->name}}</p>
                    <p><b>Location:</b> {{$event->city->name}}</p>
                    <p><b>Date/Time:</b> {{date('d F Y', strtotime($event->date))}}, {{strtoupper(date('H:i', strtotime($event->time)))}}</p>                
                    <p><b>Current players:</b> {{$event->players->where('status', 'approved')->count()}} ({{$event->game->max_players - $event->players->where('status', 'approved')->count()}} more needed)</p>
                    <input name="event_id" type="hidden" value="{{$event->id}}">
                </div>
            </div>
        </div>
    </div>
@endforeach