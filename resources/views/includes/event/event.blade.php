@foreach ($events as $event)
<div class="col-md-12">

    <div class="row event-card event-card-detailed">
            
        <div class="col-md-4">
            <div class="game-picture">
                <img class="img-responsive" src="{{asset('storage/'.$event->game->picture)}}">
            </div>
        </div>
        <div class="col-md-8 event-details-text-holder">
            <div class="event-details-text">
                <p class="event-details-p">Date: {{date('d F Y', strtotime($event->date))}} Time: {{strtoupper(date('H:i', strtotime($event->time)))}}</p>
                <p class="event-details-p">{{$event->game->name}}, ({{$event->game->min_players}}-{{$event->game->max_players}} players)</p>
                <p class="event-details-p">Location: {{$event->city->name}}</p>
                <p class="event-details-p">Host: {{$event->host->name}}</p>
                <p class="event-details-p">Current players: {{$event->players->where('status', 'approved')->count()}}</p>
                
                <div class="form-inline">
                    <input name="event_id" type="hidden" value="{{$event->id}}">
                    <p class="event-details-clicker">Event details</p>
                    <p class="event-requests-show">Requests
                        {{-- show this notification only if the event has pending requests --}}
                        @if($event->players->where('status', 'pending')->count() != 0 && $event->host_id == \Auth::id())
                            <span class="badge badge-pill badge-danger">{{$event->players->where('status', 'pending')->count()}}</span>
                        @endif
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>


@endforeach