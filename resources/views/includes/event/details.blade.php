<div class="event-details-clicked-text text-center">
    <p class="h2">Event status: {{ucfirst($event->status)}}</p>
    <hr>
    <p class="event-details-p"><b>Game:</b> {{$event->game->name}}</p>
    <p class="event-details-p"><b>Time:</b> {{strtoupper(date('H:i', strtotime($event->time)))}}</p>
    <p class="event-details-p"><b>Date:</b> {{date('d F Y', strtotime($event->date))}}</p>
    <p class="event-details-p"><b>Location:</b> {{$event->city->name}}</p>
    <p class="event-details-p"><b>Host:</b> <span class="text-decorated">{{$event->host->name}}</span></p>
    <p class="event-details-p"><b>Average length:</b> {{$event->game->avg_playing_time}} min</p>
    <br>
    <ul class="list-unstyled list-inline">
        <li><b>Min:</b> {{$event->game->min_players}}</li> 
        <li><b>Max:</b> {{$event->game->max_players}}</li>
        <li><b>Approved: {{$event->players->where('status', 'approved')->count()}}</b></li>
    </ul>

    <hr>

    @if(\Auth::id() == $event->host_id && $event->players->where('status', 'approved')->count() != 0)
    <p class="btn-event-players">Players</p>
    @endif
    
    <div class="event-players-details">    
        @if($event->players->where('status', 'approved')->count() != 0)
            <div class="event-players-details-names">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <ul class="list-unstyled">
                            @foreach($event->players->where('status', 'approved') as $player)
                            <li>{{$player->player->name}} | {{$player->ability}} | {{$player->message}}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
    </div>

</div>

