<div class="container-fluid">
    <div class="row">
        <div class="cover" style="background-image:url('{{asset('img/cover.jpg')}}')">
            <div class="cover-text-holder">
                <h1>On Board</h1>
                <h2>Meet. Play. Everybody wins.</h2>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="pull-up">
            <div class="buttons-holder">
                <a href="{{route('joinagame')}}"><button class="btn btn2 btn-success">Join a game</button></a>
                <a href="{{route('creategame')}}"><button class="btn btn2 btn-danger">Propose your own</button></a>
            </div>
        </div>
    </div>
</div>

