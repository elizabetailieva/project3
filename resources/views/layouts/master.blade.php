<!DOCTYPE html>
<html>
<head>
    @include('includes.head')
</head>
<body>
    @include('includes.navbar')
    @section('content')
    @show

    <script src="{{asset('js/jquery.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <script src="{{asset('js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('js/owl.carousel.custom.js')}}"></script>
    <script src="{{asset('js/customjs.js')}}"></script>
    <script src="{{asset('js/preview.profile.picture.js')}}"></script>
    <script src="{{asset('js/event/get.events.js')}}"></script>
    <script src="{{asset('js/event/find.events.js')}}"></script>
    <script src="{{asset('js/event/hosted.events.js')}}"></script>
    <script src="{{asset('js/event/details.event.js')}}"></script>
    <script src="{{asset('js/event/request.event.js')}}"></script>


</body>
</html>