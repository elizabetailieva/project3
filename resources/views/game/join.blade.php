@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="find-a-game-title">Join a game near you</div>
            <form action="" method="" class="form-inline">

                <select id="event_city_id" name="city_id" class="form-control custom-input">
                    <option selected disabled>Where do you wanna play?</option>
                    @foreach($cities as $city)
                        <option value="{{$city->id}}">{{$city->name}}</option>
                    @endforeach
                </select>
                
or

                <select id="event_time" name="time" class="form-control custom-input">
                    <option selected disabled>What time do you wanna play?</option>
                    @foreach($events as $event)
                        <option value="{{$event->id}}">{{$event->time}}</option>
                    @endforeach
                </select>

                <input id="findEvent" type="button" class="btn btn1 btn-danger form-control" value="Go!">

            </form>
        </div>
    </div>
</div>


<div class="container-fluid" >
    <div class="row">
        <div class="col-md-6">
            <div class="row" id="eventsCity">
                <div class="event-details-clicked-text event-not-found">
                    <p>Click on the map or use the form inputs to find events near you...</p>
                </div>      
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-10">
                    <div class="event-details-clicked" style="position:absolute; z-index:10">
                    </div>
                </div>
            </div>
        </div>   
    </div>   
</div>

<div id="map">
    {{-- show map here --}}
</div>

<script  src="{{asset('js/googlemaps/init.google.map.js')}}"></script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyApzw2LScUkEEpVIR26qWSv_zGH3TwTuLY&callback=initMap"></script>


@endsection
