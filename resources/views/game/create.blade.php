@extends('layouts.master')

@section('content')

<div class="container-gray">
    <div class="container-form">
        <h3>Add a Game</h3>
        <form enctype="multipart/form-data" method="POST" action="{{route('storegame')}}">
            @csrf
            <div class="form-group">
                <label for="picture">Picture*</label>
            <input id="picture" name="picture" class="{{ $errors->has('picture') ? ' is-invalid' : '' }}" value="{{old('picture')}}" type="file">
                @if ($errors->has('picture'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('picture') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="name">Name*</label>
                <input id="name" name="name" class="form-control custom-input2{{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{old('name')}}" type="text">
                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="description">Description*</label>
                <textarea id="description" name="description" class="form-control custom-input2{{ $errors->has('description') ? ' is-invalid' : '' }}" 
                     rows="5" cols="20">{{old('description')}}</textarea>
                @if ($errors->has('description'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="min_players">Minimum players*</label>
                <input id="min_players" name="min_players" class="form-control custom-input2{{ $errors->has('min_players') ? ' is-invalid' : '' }}" value="{{old('min_players')}}" type="number">
                @if ($errors->has('min_players'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('min_players') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="max_players">Maximum players*</label>
                <input id="max_players" name="max_players" class="form-control custom-input2{{ $errors->has('max_players') ? ' is-invalid' : '' }}" value="{{old('max_players')}}" type="number">
                @if ($errors->has('max_players'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('max_players') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="avg_playing_time">Average Playing Time in Minutes*</label>
                <input id="avg_playing_time" name="avg_playing_time" class="form-control custom-input2{{ $errors->has('avg_playing_time') ? ' is-invalid' : '' }}" value="{{old('avg_playing_time')}}" type="number">
                @if ($errors->has('avg_playing_time'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('avg_playing_time') }}</strong>
                    </span>
                @endif
            </div>
            
            <input type="submit" class="btn btn1 btn-danger form-control" value="Add game">
        </form>
    </div>
</div>

@endsection
