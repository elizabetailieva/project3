@extends('layouts.master')

@section('content')

@include('includes.jumbotrone')

<div class="container-fluid">
    <div class="row">
        <div class="container-fluid-margin">
            <h3 class="text-decorated">Popular games this week:</h3>
        </div>
    </div>
    <div class="row">
        <div class="loop owl-carousel">
            @foreach($games as $game)
                <div class="game-card item">
                    <div class="game-picture">
                        <img class="img-responsive" src="{{asset('storage/'.$game->picture)}}">
                    </div>
                    <div class="game-name text-center">
                        <p>{{$game->name}}</p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>

@endsection
