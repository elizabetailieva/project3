@foreach($event_request as $request)   
    <div class="event-details-clicked-text">
        <div class="row">
            <div class="col-md-8">
                <p class="h3">Request to join from {{$request->player->name}}</p>
                <p class="event-details-p"><b>Ability:</b> {{$request->ability}}</p>
                <p class="event-details-p"><b>Message:</b> {{$request->message}}</p>
            </div>
            <div class="col-md-4 buttons-holder-form">
                <form method="POST" action="{{route('confirmrequest', ['id' => $request->id])}}">
                    @csrf
                    <input type="submit" value="Approve" id="confirm_request" class="form-control btn btn1 btn-success">
                </form>
                <br>
                <form method="POST" action="{{route('rejectrequest', ['id' => $request->id])}}">
                    @csrf
                    <input type="submit" value="Reject" id="reject_request" class="form-control btn btn1 btn-danger">
                </form>
            </div>
        </div>
    </div>
@endforeach