@extends('layouts.master')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                @foreach ($events as $event)
                    <div class="col-md-10 col-md-offset-2">
                        <div class="event-card">
                            <div class="game-card">
                                <div class="game-picture">
                                    <img class="img-responsive" src="{{asset('storage/'.$event->game->picture)}}">
                                </div>
                            </div>
                            <div class="event-details-text-holder">
                                <div class="event-details-text">
                                    <h3>{{date('d F Y', strtotime($event->date))}}, {{strtoupper(date('H:i', strtotime($event->time)))}}</h3>
                                    <h4>{{$event->game->name}}, ({{$event->game->min_players}}-{{$event->game->max_players}} players)</h4>
                                    <h4>Location: {{$event->city->name}}</h4>
                                    <h4>Host: {{$event->host->name}}</h4>
                                    <h4>Current players: </h4>
                                    <input name="event_id" type="hidden" value="{{$event->id}}">
                                    <h3 class="event-details-clicker">View event details &#8594</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-10">
                    <div class="event-details-clicked">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection