<div class="event-details-clicked-text">
    <p class="h2">Want to join?</p>
        <form method="POST" action="{{route('eventmessagepost')}}">
            @csrf
            <div class="row">
                @if ($errors->has('message'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('message') }}</strong>
                    </span> 
                @endif
                <div class="col-md-5">
                    <label for="message">Message</label>
                </div>
                <div class="col-md-4">
                    <label for="ability">Ability</label>          
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <input type="text" name="message" id='message' class="form-control">
                </div>
                <div class="col-md-4">
                    <select name="ability" id='ability' class="form-control">
                        <option value="Newborn">Newborn</option>
                        <option value="Cave troll">Cave troll</option>
                        <option value="Mage">Mage</option>
                        <option value="Minotaur">Minotaur</option>
                        <option value="Warlord">Warlord</option>
                    </select>
                </div>
                <input type="hidden" name="event_id" value="{{$event_id}}">
                <div class="col-md-3">
                    <input type="submit" value="Send" class="form-control btn btn-danger">
                </div>
            </div>
        </form>    
</div>