@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h2>My Events</h2>
        </div>
    </div>
    
    @if($flash = session('message'))
        <div class="row" id="user_registered_holder">
            <div class="col-md-8 col-md-offset-2">
                <div class="alert alert-success" id="user_registered">
                    <strong>{{$flash}}</strong>
                </div> 
            </div>
        </div>
    @endif
            
    <div class="row"> 
        <div class="col-md-8 col-md-offset-2">
            <ul class="list-inline my-events-menu">
                <li class="my-events-menu-item active" id="requested">Requested</li><li class="my-events-menu-item" id="hosted">Hosted
                    @if($count != 0) 
                        <span class="badge badge-pill badge-danger">{{$count}}</span>
                    @endif
                </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="row">
                <div id="event-details">
                </div>
            </div>
        </div>
    </div>

    <div class="event-details-clicked"></div>
    
</div>

@endsection