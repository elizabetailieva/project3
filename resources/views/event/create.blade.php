@extends('layouts.master') 
@section('content')

<div class="container-gray">
    <div class="container-form">
        <h3>Create an Event</h3>
        <form method="POST" action="{{route('storeevent')}}">
            @csrf
            <div class="form-group">
                <label for="date">Date*</label>
                <input id="date" name="date" class="form-control custom-input{{ $errors->has('date') ? ' is-invalid' : '' }}" value="{{ old('date') }}" type="date">                
                @if ($errors->has('date'))
                <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('date') }}</strong>
                    </span> @endif
            </div>
            <div class="form-group">
                <label for="time">Time*</label>
                <input id="time" name="time" class="form-control custom-input{{ $errors->has('time') ? ' is-invalid' : '' }}" value="{{ old('time') }}" type="time">                
                @if ($errors->has('time'))
                <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('time') }}</strong>
                    </span> 
                    @endif
            </div>
            <div class="form-group">
                <label for="location">Location*</label>
                <select id="location" name="city_id" class="form-control custom-input{{ $errors->has('city_id') ? ' is-invalid' : '' }}">
                    <option selected disabled>Location</option>
                    @foreach($cities as $city)
                        <option value="{{$city->id}}" {{ old('city_id') == $city->id ? "selected" : ""}} >{{$city->name}}</option>
                    @endforeach
                </select>
                @if ($errors->has('city_id'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('city_id') }}</strong>
                    </span> 
                @endif
            </div>
            <div class="form-group">
                <label for="game">Game</label>
                <select id="game" name="game_id" class="form-control custom-input{{ $errors->has('game_id') ? ' is-invalid' : '' }}">
                    <option selected disabled>Select a game</option>
                    @foreach($games as $game)
                        <option value="{{$game->id}}" {{ old('game_id') == $game->id ? "selected" : ""}} >{{$game->name}}</option>
                    @endforeach
                </select>
                @if ($errors->has('game_id'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('game_id') }}</strong>
                    </span> 
                @endif
            </div>
            <input type="submit" class="btn btn1 btn-danger form-control" value="Create event">
        </form>
    </div>
</div>
@endsection