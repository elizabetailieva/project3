<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyEnumInEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            DB::statement("ALTER TABLE events MODIFY status ENUM('pending', 'full', 'finished', 'canceled') NOT NULL DEFAULT 'pending'");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            DB::statement("ALTER TABLE events MODIFY status ENUM('pending', 'finished', 'canceled') NOT NULL DEFAULT 'pending'");
        });
    }
}
