<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Event;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('home', function($view){
            $view->with('games', \App\Game::all());
        });

        view()->composer('event.user', function($view){
            $view->with('count', 
            Event::whereHas('players', function($query){
                    $query->where('status', 'pending');
                })->where('host_id', \Auth::id())->count());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
