<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Event;
use App\User;
use App\EventRequest;
use Carbon\Carbon as Carbon;


class EventRequestController extends Controller
{
    public function eventmessageget(Request $request)
    {
        $event_id = $request->event_id;
        
        $event = Event::findOrFail($request->event_id);
        $user_id = Auth::id();
        
        $count = EventRequest::where('player_id', $user_id)->where('event_id', $event_id)->count();

        //if count is not zero user has sent request for this event
        if($count != 0){
            return view('event.requestdenied');     
        } else {
            return view('event.request', ['event_id' => $event_id]);
        }
    }

    public function eventmessagepost(Request $request)
    {
        $event_request = new EventRequest;
        $event_request->event_id = $request->event_id;
        $event_request->player_id = Auth::id();
        $event_request->message = $request->message;
        $event_request->ability = $request->ability;
        $event_request->created_at = Carbon::now();
        $event_request->updated_at = Carbon::now();
        $event_request->save();

        session()->flash('message', 'Event request sent!');

        return redirect()->route('userevents');    
    }

    public function eventrequestdetails(Request $request){
        $event = $request->event_id;

        $event_request = EventRequest::where('event_id', $event)->where('status', 'pending')->with('player')->get();

        if(count($event_request) != 0){
            return view('event.requesteddetails', ['event_request' => $event_request]);
        } else {
            return view('event.norequests');
        }
        
    }

    public function confirmrequest(Request $request){
        EventRequest::where('id', $request->id)->update(['status' => 'approved']);

        return redirect()->home();
    }

    public function rejectrequest(Request $request){

        EventRequest::where('id', $request->id)->update(['status' => 'rejected']);

        return redirect()->home();
    }

}
