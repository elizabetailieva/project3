<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;
use App\Event;

class MapOfEventsController extends Controller
{
    public function joinagame()
    {
        $events = Event::all();
        $city_id = Event::select('city_id')->distinct()->get();
        $cities = City::whereIn('id', $city_id)->get();
        return view('game.join', ['events' => $events, 'cities' => $cities]);
    }

    public function findeventonmap(Request $request) {

        $lat = [
            'zh','dz','lj','nj','gj','ts','ch','dz','sh','kj','Gj','Zh','Dz','Lj','Nj','Kj','Ts','Ch','Dz','Sh','Ё',     
            'a','b','v','g','d','e','z','i','ј','k','l','m','n','o','p','r','s','t','u','f','h', 'c', 'ë',
            'A','B','V','G','D','E','Z','I','J','K','L','M','N','O','P','R','S','T','U','F','H','C'
        ];

        $cyr = [
            'ж','ѕ','љ','њ','ѓ','ц','ч','џ','ш','ќ', 'Ѓ','Ж','Ѕ','Љ','Њ','Ќ','Ц','Ч','Џ','Ш', 'O',
            'а','б','в','г','д','е','з','и','ј','к','л','м','н','о','п','р','с','т','у','ф','х','ц','o',
            'А','Б','В','Г','Д','Е','З','И','J','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ц'
        ];
    
        $city = str_replace($lat, $cyr, $request->cityName);
        $user = \Auth::id();

        $events = Event::whereHas('city', function ($query) use($city, $user) {
            $query->where('name', $city)->where('host_id', '!=', $user);
        })->get();

        if (count($events) != 0) {
            return response()->view('includes.event.basic', ['events' => $events]);
        }
        else {
            return response()->view('event.eventdoesntexist');
        }

        
    }

    public function findeventinputselect(Request $request) {
        $user = \Auth::id();

        $events = Event::where('city_id', '=', $request->city_id)->where('host_id', '!=', $user)->orWhere('id', '=', $request->event_time)->get();

        if (count($events) != 0) {
            return response()->view('includes.event.basic', ['events' => $events]);
        }
        else {
            return response()->view('event.eventdoesntexist');
        }
    }
}
