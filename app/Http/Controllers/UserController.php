<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function profile(){
        return view('auth.profile', ['user' => Auth::user()]);
    }

    public function update_avatar(Request $request){


        $this->validate($request, [
            'avatar' => 'required|image|max:2048',
        ]);

        $avatar = $request->file('avatar');
        $filename = time() . '.' . $avatar->getClientOriginalExtension();
        \Image::make($avatar)->resize(300, 300)->save( public_path('/img/' . $filename ) );
        $user = Auth::user();
        $user->avatar = $filename;
        $user->save();
    

    	return view('auth.profile', array('user' => Auth::user()) );

    }
}
