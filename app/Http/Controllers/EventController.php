<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Game;
use App\City;
use App\Event;
use Carbon\Carbon as Carbon;

use App\Http\Requests\EventForm;
use Illuminate\Support\Facades\Auth;
use App\EventRequest;

class EventController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function create()
    {
        $cities = City::all();
        $games = Game::all();
        return view('event.create', ['cities' => $cities, 'games' => $games]);
    }

    public function store(EventForm $form)
    {
        $form->persist();
        return redirect()->route('home');
    }

    public function eventdetails(Request $request)
    {
        $event = Event::findOrFail($request->event_id);
        return view('includes.event.details', ['event' => $event])->render();
    }

    public function userevents() 
    {
        return view('event.user');
    }

    public function hostedevents(Request $request) 
    {
        $host = Auth::id();
        $events = Event::where('host_id', $host)->get();
        return view('includes.event.event', ['events' => $events]);
    }

    public function requestedevents(Request $request) 
    {
        $player = Auth::id();
        
        $events = Event::wherehas('players', function($query) use($player) {
            $query->where('player_id', $player)->where('host_id', '!=', $player);
        })->get();

        return view('includes.event.requested', ['events' => $events]);
    }

        
}
