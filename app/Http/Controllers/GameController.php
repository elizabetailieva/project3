<?php

namespace App\Http\Controllers;


use App\Http\Requests\GameForm;

class GameController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        return view('game.create');
    }

    public function store(GameForm $form)
    {
        $form->persist();
        
        return redirect()->route('home');
    }
}
