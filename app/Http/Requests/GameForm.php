<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Game;

class GameForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'picture' => 'file|required',
            'name' => 'required|string',
            'description' => 'required|string|max:250',
            'min_players' => 'required|integer',
            'max_players' => 'required|integer',
            'avg_playing_time'=> 'required|integer'
        ];
    }
    
    public function messages()
    {
        return [
            'picture.file' => 'Please upload a picture',
            'picture.required' => 'Picture of game is required',
            'description.required' => 'Description of game is required',
            'description.max' => 'Description of game cannot exceed 250 caracters',
            'min_players.required' => 'Minimum number of players is required',
            'min_players.integer' => 'Minimum number of players must be a number',
            'max_players.required' => 'Maximum number of players is required',
            'max_players.integer' => 'Maximum number of players must be a number',
            'avg_playing_time.required' => 'Average playing time of game is required'
        ];
    }

    public function persist()
    {
        $game = new Game();
        $game->picture = $this->picture->store('games');
        $game->name = $this->name;
        $game->description = $this->description;
        $game->min_players = $this->min_players;
        $game->max_players = $this->max_players;
        $game->avg_playing_time = $this->avg_playing_time;
        $game->save();
    }
}
