<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use App\Event;

class EventForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required',
            'time' => 'required',
            'city_id' => 'required',
            'game_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'date.required' => 'Date of event is required',
            'time.required'  => 'Time of event is required',
            'city_id.required'  => 'Location of event is required',
            'game_id.required'  => 'Game of event is required',
        ];
    }
    

    public function persist()
    {
        $event = new Event();
        $event->date = $this->date;
        $event->time = $this->time;
        $event->city_id = $this->city_id;
        $event->game_id = $this->game_id;
        $event->host_id = Auth::id();
        $event->save();
    }
}
