<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventRequest extends Model
{
    public function player(){
        return $this->belongsTo(User::class);
    }

    public function event(){
        return $this->belongsTo(Event::class);
    }
}
