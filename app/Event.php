<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{

    public function game(){
        return $this->belongsTo(Game::class);
    }

    public function players()
    {
        return $this->hasMany(EventRequest::class);
    }

    public function host(){
        return $this->belongsTo(User::class);
    }

    public function city(){
        return $this->belongsTo(City::class);
    }
    
}
