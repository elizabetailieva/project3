
function addMarkersToMap(map) {
  var skopjeMarker = new H.map.Marker({lat:41.99646, lng:21.43141});
  map.addObject(skopjeMarker);

  var velesMarker = new H.map.Marker({lat:41.724182, lng: 21.774216});
  map.addObject(velesMarker);

  var kavadarciMarker = new H.map.Marker({lat:41.43306, lng:22.01194});
  map.addObject(kavadarciMarker);

} 
  /**
   * Boilerplate map initialization code starts below:
   */
  // Step 1: initialize communication with the platform
  var platform = new H.service.Platform({
    app_id: 'kSvooOD4O8K5cxEG5NAi',
    app_code: '5JofbNvox-x9egt0mTKwiQ',
    useHTTPS: true,
    useCIT: true
  });
  var defaultLayers = platform.createDefaultLayers();
  
  var mapContainer = document.getElementById('map');
  
  // Step 2: initialize a map
  var map = new H.Map(mapContainer, defaultLayers.normal.map, {
    // initial center and zoom level of the map
    zoom: 8.5,
    // Veles
    center: {lat: 41.724182, lng: 21.774216}
  });
  
  // Step 3: make the map interactive
  // MapEvents enables the event system
  // Behavior implements default interactions for pan/zoom (also on mobile touch environments)
  var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));
  
  // Step 4: Create the default UI
  var ui = H.ui.UI.createDefault(map, defaultLayers, 'en-US');
  
  addMarkersToMap(map);