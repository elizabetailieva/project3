$(document).ready(function(){
    //start owl-carousel on page load
    $(".owl-carousel").owlCarousel();
});

//owl-carousel settings
$('.loop').owlCarousel({
    center: true,
    items:2,
    loop:true,
    margin:10,
    autoplay:true,
    autoplayTimeout:4000,
    autoplayHoverPause:true,
    responsive:{
        0:{
            items:2
        },
        700:{
            items:3
        },
        950: {
            items:4
        },
        1200:{
            items:5
        },
        1400: {
            items:6
        }
    }
});
