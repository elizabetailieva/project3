//find event by click on map
 
function initMap() {
    var myLatlng = {lat: 41.6, lng: 21.7};

    //initialize a new google maps
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 8.5,
        center: myLatlng
    });

    var marker;

    map.addListener('click', function(event) {

        //remove the previous marker if exists
        if (marker != undefined) {
            marker.setMap(null);
        }

        //add a new marker on click on map
        createMarker(event.latLng, map);

        var geocoder = new google.maps.Geocoder();
       
        geocoder.geocode({
            'latLng': event.latLng,
            'region': 'MK',
            'language': 'en'
        }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    var address = results[0].address_components;

                    $(address).each(function(){
                        if($(this)[0]['types'][0] == 'locality'){
                
                            //on click, with Geocoder take city name in latin letters
                            var cityName = ($(this)[0]['long_name']);

                            //send ajax request to a controller where latin letters are transliterated in cirillyc letters so the events can be filtered in database by city name
                            $.ajax({
                                type: 'GET',
                                url: '/event/city',
                                data: {
                                    cityName: cityName
                                }
                            }).done(function(data){
                                   $('#eventsCity').html(data);
                            }).fail(function(error){
                                    console.log('error');
                            });
                        }
                    });
                }
            }
        });           
    });

    //function that creates markers on map, accepts position and map as argument
    function createMarker(position, map){
        marker = new google.maps.Marker({
            position: position, 
            map: map
        });
    }
}