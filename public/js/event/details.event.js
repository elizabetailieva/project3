//show event details on click
$(function(){
    var loadedinfo = false;
    $(document).on('click', '.event-details-clicker', function(){
        var event_id = $(this).siblings("input[name=event_id]").val();

        if(loadedinfo){
            $('.event-details-clicked').hide();
            loadedinfo = false;
            return;
        }

        event.stopPropagation();

        $.ajax({
            type: "GET",
            url: '/event/details',
            data: {
                event_id: event_id,
            }
        }).done(function(data){
            $('.event-details-clicked').html(data);
            $('.event-details-clicked').show();
            loadedinfo = true;
        }).fail(function(error){
            console.log('error');
        });
    }); 


    // hide div when clicking outside of it

    $(document).mouseup(function(e) 
    {
        var container = $(".event-details-clicked");

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0) 
        {
            container.hide();
            loadedinfo = false;
        }
    });


    $(document).on('click', '.btn-event-players', function(){
        $('.event-players-details').slideToggle();
    });

});
        
