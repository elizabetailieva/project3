 // show form to request an event on click 

 $(function(){
    var loaded = false;
     
    $(document).on('click', '.event-card-basic', function(event){

        if(loaded){
            $('.event-details-clicked-text').remove();
            loaded = false;
            return;
        }

        var element = this;
        var event_id = $(this).find('input[name=event_id]').val();

        $.ajax({
            type: "GET",
            url: '/event/message',
            data: {
                event_id: event_id,
            }
        }).done(function(data){
           $(element).after(data);
            loaded = true;
        }).fail(function(error){
            console.log('error');
        });
    });

   // hide div when clicking outside of it

    $(document).mouseup(function(e) 
    {
        var container = $(".event-details-clicked-text");

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0) 
        {
            container.hide();
            loaded = false;
        }
    });

});

