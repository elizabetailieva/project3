//find events by submitting form inputs
$(function(){
    $('#findEvent').on('click', function(){
        var city_id = $('#event_city_id').val();
        var event_time = $('#event_time').val();
        $.ajax({
            type: "GET",
            url: '/event/find',
            data: {
                city_id: city_id,
                event_time: event_time
            }
        }).done(function(data){
            $('#eventsCity').html(data);
        }).fail(function(error){
            console.log('error');
        });
    })
})