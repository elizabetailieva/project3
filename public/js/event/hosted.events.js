$(function(){
    var loaded = false;
    //show event requests below hosted events
    $(document).on('click', '.event-requests-show', function(){

        if(loaded){
            $('.event-details-clicked-text').remove();
            loaded = false;
            return;
        } else {
            //if it has class hosted, it means that hosted is clicked
            if( $('#event-details').hasClass('hosted-events')){
                var element = $(this).closest('.event-card-detailed');
                var event_id = $(this).siblings('input[name=event_id]').val();

                $.ajax({
                    type: "GET",
                    url: '/event/request/details',
                    data: {
                        event_id: event_id,
                    }
                }).done(function(data){
                    $(element).after(data);
                    loaded = true;                    
                }).fail(function(error){
                    console.log('error');
                });
            }
        }
        
    });

})