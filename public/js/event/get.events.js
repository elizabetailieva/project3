//show hosted or requested events
$(function(){

    $(document).ready(function(){

        if (top.location.pathname === '/event/user')
        {
            ajaxGet('/event/requested');
        }
        
    })
   
    $('#hosted').on('click', function(){
        
        $(this).addClass('active');
        $('#event-details').addClass('hosted-events');
        
        if($('#requested').hasClass('active')){
            $('#requested').removeClass('active');
        }

        ajaxGet('/event/hosted'); 
        
    })

    $('#requested').on('click', function(){

        $(this).addClass('active');

        if($('#hosted').hasClass('active')){
            $('#hosted').removeClass('active');
            $('#event-details').removeClass('hosted-events');
        }
        
        ajaxGet('/event/requested');       
    })

    function ajaxGet(url) {
        $.ajax({
            type: 'GET',
            url: url
        }).done(function(data){
                $('#event-details').html(data);
        }).fail(function(error){
                console.log('fail');
        })
    }

})



